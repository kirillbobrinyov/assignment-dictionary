OBJDIR = obj
BUILDDIR = build
ASMFLAGS =-felf64 -g
RM = rm -rf

all: $(BUILDDIR)/program

$(OBJDIR)/dict.o: dict.asm
	nasm $(ASMFLAGS) -o $@ $<

$(OBJDIR)/lib.o: lib.asm
	nasm $(ASMFLAGS) -o $@ $<

$(BUILDDIR)/program: $(OBJDIR)/main.o $(OBJDIR)/lib.o $(OBJDIR)/dict.o
	ld -o $@ $^

$(OBJDIR)/main.o: main.asm words.inc colon.inc
	nasm $(ASMFLAGS) -o $@ $<

.PHONY: clean
clean: 
	$(RM) $(OBJDIR) $(BUILDDIR)
