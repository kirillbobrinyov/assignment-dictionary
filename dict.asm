%include "lib.inc"

global find_word

section .text

find_word:   
.cnt:    
    test rsi, rsi
    jz .stop

    push rsi
    push rdi
    add rsi, 8
    call string_equals
    pop rdi
    pop rsi

    cmp rax, 1
    je .stop
    mov rsi, [rsi]
    jmp .cnt
    
.stop:
    mov rax, rsi
    ret
