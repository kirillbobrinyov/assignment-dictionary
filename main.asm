%include "words.inc"
%include "lib.inc"

extern find_word

%define STDOUT 1
%define STDERR 2
%define BUFFER 256

section .rodata
no_key_msg: db "No information about this key in a dictionary.", 0xA, 0
err_msg: db "Wrong input.", 0XA, 0

section .text
global _start

_start:
    sub rsp, BUFFER
    mov rdi, rsp
    mov rsi, BUFFER
    call read_phrase
    test rax, rax
    jz .error

    mov rsi, PREV_IND
    mov rdi, rsp
    call find_word
    test rax, rax
    jz .no_such_key

.success:    
    add rsp, BUFFER
    add rax, 8
    mov rdi, rax
    mov rsi, STDOUT
    call string_length
    inc rax
    add rdi, rax
    call print_string
    call print_newline
    xor rdi, rdi
    jmp .end

.error:
    mov rdi, err_msg
    mov rsi, STDERR
    call print_string
    mov rdi, 1
    jmp .end
.no_such_key:
    mov rdi, no_key_msg
    mov rsi, STDERR
    call print_string
    mov rdi, 1
.end:
    jmp exit
